import "reflect-metadata";
import { createConnection } from 'typeorm';

export const databaseProviders = [
  {
    provide: 'DbConnectionToken',
    useFactory: async () => await createConnection({
      type: 'postgres',
      host: process.env.RAILS_DATABASE_HOST,
      port: 3306,
      username: process.env.RAILS_DATABASE_USERNAME,
      password: process.env.RAILS_DATABASE_PASSWORD,
      database: process.env.RAILS_DATABASE_NAME,
      entities: [
        __dirname + '/../**/*.entity{.ts,.js}',
      ],
      synchronize: true,
    }),
  },
];