
## Autocannon

### BASIC TEST

```
# Phoenix
autocannon -c 8 -d 10 -p 1 http://localhost:3003/api/v1/contacts
# Dotnet
autocannon -c 8 -d 10 -p 1 http://localhost:3002/api/v1/contacts
# Express + Objection.js
autocannon -c 8 -d 10 -p 1  http://localhost:3001/api/v1/contacts
# Express + Sequelize
autocannon -c 8 -d 10 -p 1  http://localhost:3001/api/v2/contacts
# Express + TypeORM
autocannon -c 8 -d 10 -p 1  http://localhost:3001/api/v3/contacts
# Rails
autocannon -c 8 -d 10 -p 1 http://localhost:3000/api/v1/contacts
```

### CONCURRENCY TEST

```
# Phoenix
autocannon -c 80 -d 10 -p 8  http://localhost:3003/api/v1/contacts
# Dotnet
autocannon -c 80 -d 10 -p 8  http://localhost:3002/api/v1/contacts
# Express + Objection.js
autocannon -c 80 -d 10 -p 8  http://localhost:3001/api/v1/contacts
# Express + Sequelize
autocannon -c 80 -d 10 -p 8  http://localhost:3001/api/v2/contacts
# Express + TypeORM
autocannon -c 80 -d 10 -p 8  http://localhost:3001/api/v3/contacts
# Rails
autocannon -c 80 -d 10 -p 8  http://localhost:3000/api/v1/contacts

```