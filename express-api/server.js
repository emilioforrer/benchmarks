const express = require('express')
const bodyParser = require('body-parser');
const ObjectionORM = require('./orms/models/objection');
const SequelizeORM = require('./orms/models/sequelize');
const TypeORM = require('./orms/models/typeorm');

const app = express()
// const port = 'tmp/express.benchmarks.sock'
const port = 3000

app.use(bodyParser.json())
// app.set('trust proxy', true);


 

app.get('/api/v1/contacts', async (req, res) => {
  const contacts = await ObjectionORM.Contact.query()
    .eager('[emails, phones, addresses]')
  res.send({
    data: contacts
  })
})

app.get('/api/v2/contacts', async (req, res) => {
  const contacts = await SequelizeORM.Contact.findAll({
    include: [
      {
        model: SequelizeORM.Address,
        as: 'addresses'
      },
      {
        model: SequelizeORM.Phone,
        as: 'phones'
      },
      {
        model: SequelizeORM.Email,
        as: 'emails'
      }
    ]
  })
  res.send({
    data: contacts
  })
})

app.get('/api/v3/contacts', async (req, res) => {
  const Contact = TypeORM.conn.getRepository(TypeORM.Contact)
  const contacts = await Contact.find({
    relations: ['emails', 'phones', 'addresses']
  })
  res.send({
    data: contacts
  })
})

app.use((err, req, res, next) => {
  if (err) {
    res.status(err.statusCode || err.status || 500).send(err.data || err.message || {});
  } else {
    next();
  }
});

app.listen(port, async () => {
  await TypeORM.connect()
  console.log("Application connected on port " + port);
})