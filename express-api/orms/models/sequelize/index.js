const Sequelize = require('sequelize');


const sequelize = new Sequelize(process.env.RAILS_DATABASE_NAME, process.env.RAILS_DATABASE_USERNAME, process.env.RAILS_DATABASE_PASSWORD, {
  host: process.env.RAILS_DATABASE_HOST,
  dialect: 'postgres',
  logging: false,
  pool: {
    max: 8,
    min: 0
  },

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  // operatorsAliases: false
});

const Phone = require('./Phone')(sequelize, Sequelize)

const Address = require('./Address')(sequelize, Sequelize)

const Email = require('./Email')(sequelize, Sequelize)


const Contact = require('./Contact')(sequelize, Sequelize)


Contact.hasMany(Email, {
  as: 'emails',
  foreignKey: 'contact_id',
  sourceKey: 'id'
})

Contact.hasMany(Phone, {
  as: 'phones',
  foreignKey: 'contact_id',
  sourceKey: 'id'
})

Contact.hasMany(Address, {
  as: 'addresses',
  foreignKey: 'contact_id',
  sourceKey: 'id'
})

module.exports = {
  Contact,
  Phone,
  Address,
  Email
}