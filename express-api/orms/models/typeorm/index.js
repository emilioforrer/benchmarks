const typeorm = require("typeorm")
const createConnection = typeorm.createConnection
const Address = require('./Address')
const Contact = require('./Contact')
const Email = require('./Email')
const Phone = require('./Phone')

const orm = {
  conn: null,
  Contact,
  async connect() {
    this.conn = await createConnection({
      type: "postgres",
      host: process.env.RAILS_DATABASE_HOST,
      username: process.env.RAILS_DATABASE_USERNAME,
      password: process.env.RAILS_DATABASE_PASSWORD,
      database: process.env.RAILS_DATABASE_NAME,
      port: 5432,
      extra: {
        pool: {
          max: 8,
          min: 0
        }
      },
      entities: [
        Address,
        Contact,
        Email,
        Phone
      ]
    })
    return this.conn
  }
}
module.exports = orm