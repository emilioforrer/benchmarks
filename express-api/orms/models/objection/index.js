const {
  Model
} = require('objection');
const Knex = require('knex');
const knexConfig = {
  client: 'pg',
  //version: '7.2',
  connection: {
    host: process.env.RAILS_DATABASE_HOST,
    user: process.env.RAILS_DATABASE_USERNAME,
    password: process.env.RAILS_DATABASE_PASSWORD,
    database: process.env.RAILS_DATABASE_NAME
  },
  pool: {
    min: 1,
    max: 8
  },
  acquireConnectionTimeout: 10000
}

// Initialize knex.
const knex = Knex(knexConfig);

// Give the knex object to objection.
Model.knex(knex);

const Contact = require('./Contact')

module.exports = {
  Contact
}