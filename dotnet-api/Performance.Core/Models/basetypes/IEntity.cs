﻿using System;

namespace Performance.Core.Models.basetypes
{
    public interface IEntity
    {
        long Id { get; set; }
        string Uuid { get; set; }
        DateTime CreatedAt { get; set; }
        DateTime UpdatedAt { get; set; }
    }
}