﻿using System;
using System.Collections.Generic;
using System.Text;
using Performance.Core.Models.basetypes;

namespace Performance.Core.Models
{
    public class Contact : IEntity
    {
      

        public Contact()
        {
            Addresses = new List<Address>();
            Phones = new List<Phone>();
            Emails = new List<Email>();
        }

        public long Id { get; set; }
        public string Uuid { get; set; }
        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public IEnumerable<Address> Addresses { get; private set; }
        public IEnumerable<Phone> Phones { get; private set; }
        public IEnumerable<Email> Emails { get; private set; }

    }
}
