﻿using System;
using System.Collections.Generic;
using System.Text;
using Performance.Core.Models.basetypes;

namespace Performance.Core.Models
{
    public class Email : IEntity
    {
        public long Id { get; set; }
        public string Uuid { get; set; }
        public long ContactId { get; set; }
        public string Account { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
