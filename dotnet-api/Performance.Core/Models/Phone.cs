﻿using System;
using Performance.Core.Models.basetypes;

namespace Performance.Core.Models
{
    public class Phone : IEntity
    {
        public long Id { get; set; }
        public string Uuid { get; set; }
        public string Number { get; set; }
        public long ContactId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}