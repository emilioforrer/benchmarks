﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Performance.Core.Models;

namespace Performance.Data.Mappings
{
    public class ContactMap : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.ToTable("contacts");
            builder.HasKey(x => x.Id);
            builder.HasAlternateKey(x => x.Uuid);
            builder.Property(x => x.Id).HasColumnName("id");
            builder.Property(x => x.Uuid).HasColumnName("uuid");
            builder.Property(x => x.Name).HasColumnName("name").IsRequired();
            builder.Property(x => x.CreatedAt).HasColumnName("created_at").IsRequired();
            builder.Property(x => x.UpdatedAt).HasColumnName("updated_at");

            


            builder.HasMany(x => x.Addresses).WithOne().HasForeignKey(x => x.ContactId);
            builder.HasMany(x => x.Phones).WithOne().HasForeignKey(x => x.ContactId);
            builder.HasMany(x => x.Emails).WithOne().HasForeignKey(x => x.ContactId);

        }
    }
}