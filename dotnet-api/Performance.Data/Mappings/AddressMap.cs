﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Performance.Core.Models;

namespace Performance.Data.Mappings
{
    public class AddressMap : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable("addresses");
            builder.HasKey(x => x.Id);
            builder.HasAlternateKey(x => x.Uuid);
            builder.Property(x => x.Id).HasColumnName("id");
            builder.Property(x => x.Uuid).HasColumnName("uuid");
            builder.Property(x => x.Description).HasColumnName("description").IsRequired();
            builder.Property(x => x.ContactId).HasColumnName("contact_id").IsRequired();
            builder.Property(x => x.CreatedAt).HasColumnName("created_at").IsRequired();
            builder.Property(x => x.UpdatedAt).HasColumnName("updated_at");
        }
    }
}