#!/bin/bash
set -e
nginx -t
service nginx restart
dotnet Performance.Web.dll

exec "$@"