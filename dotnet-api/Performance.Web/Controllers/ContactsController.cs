﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Performance.Core.Models;
using Performance.Data;
using Performance.Web.Helpers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Performance.Web.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ContactsController : Controller
    {
        //test
        private readonly ContactDbContext _context;

        public ContactsController(ContactDbContext context)
        {
            _context = context;
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<GenericDto<Contact>> Get()
        {
            var list = await  _context.Contacts
                .Include(x => x.Addresses)
                .Include(x => x.Emails)
                .Include(x => x.Phones)
                .ToListAsync();

            return new GenericDto<Contact>(list);
        }

        // GET api/<controller>/5
        
    }
}
