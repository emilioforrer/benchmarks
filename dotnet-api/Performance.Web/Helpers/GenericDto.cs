﻿using System.Collections.Generic;

namespace Performance.Web.Helpers
{
    public class GenericDto<TType>
    where TType: class,new()
    {
        public GenericDto(List<TType> data)
        {
            Data = data;
        }
        public  List<TType> Data { get; private set; }
    }
}