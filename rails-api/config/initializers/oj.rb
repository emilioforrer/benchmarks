require 'active_support/core_ext'
require 'active_support/json'
require 'oj'
Oj.default_options = { mode: :rails, bigdecimal_as_decimal: true, bigdecimal_load: :bigdecimal }
Oj.optimize_rails
MultiJson.engine = :oj