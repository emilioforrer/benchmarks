Contact.transaction do
  1000.times do
    contact  = Contact.new
    contact.name = Faker::Name.name_with_middle
    2.times do
      address = contact.addresses.build
      address.description = Faker::Address.street_address
      phone = contact.phones.build
      phone.number = Faker::PhoneNumber.phone_number
      email = contact.emails.build
      email.account = Faker::Internet.email
    end
    contact.save
  end
end