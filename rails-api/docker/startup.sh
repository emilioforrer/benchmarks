#!/bin/bash
cp $APP_DIR/docker/Procfile .

if [[ $STAGE == "production" || $STAGE == "staging" ]]; then
  bundle exec rails db:create
  bundle install --jobs=10 --without development test
else
  bundle install --jobs=10
fi

bundle exec rails db:migrate

# bundle exec passenger start --min-instances 8
# bundle exec iodine -p $PORT -t $RAILS_MAX_THREADS -w $WEB_CONCURRENCY
# bundle exec falcon serve --port $PORT --concurrency $RAILS_MAX_THREADS -b http://0.0.0.0 
foreman start
# bundle exec puma -C config/puma.rb

exec "$@"