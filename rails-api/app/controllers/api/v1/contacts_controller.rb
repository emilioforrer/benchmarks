class Api::V1::ContactsController < ActionController::API
  def index
    cache = ENV['ENABLE_CACHE']
    if cache
      json = Rails.cache.fetch('contacts', expires_in: 24.hours) do
        contacts_json
      end
    else
      json = contacts_json
    end

    render json: "{data:#{json}}"
  end

  protected 

  def jbuilder
    Jbuilder.new do |json|
      json.data do
        json.array! contacts do |contact|
          json.id contact.id
          json.uuid contact.uuid
          json.name contact.name
          json.created_at contact.created_at
          json.updated_at contact.updated_at
          json.emails do
            json.array! contact.emails do |email|
              json.id email.id
              json.uuid email.uuid
              json.account email.account
              json.contact_id email.contact_id
              json.created_at email.created_at
              json.updated_at email.updated_at
            end
          end

          json.phones do
              json.array! contact.phones do |phone|
              json.id phone.id
              json.uuid phone.uuid
              json.number phone.number
              json.contact_id phone.contact_id
              json.created_at phone.created_at
              json.updated_at phone.updated_at
            end
          end

          json.addresses do
            json.array! contact.addresses do |address|
              json.id address.id
              json.uuid address.uuid
              json.description address.description
              json.contact_id address.contact_id
              json.created_at address.created_at
              json.updated_at address.updated_at
            end
          end
        end
      end
    end
  end


  def contacts_json
    contacts.to_json(
      include: [
        :emails,
        :addresses,
        :phones
      ]
    )
  end

  def contacts
    @contacts ||= Contact.all.includes(
      :emails,
      :addresses,
      :phones
    )
  end
end
