class Contact < ApplicationRecord
  include Concerns::Common
  has_many :addresses
  has_many :phones
  has_many :emails
end
