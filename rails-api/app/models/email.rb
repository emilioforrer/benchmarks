class Email < ApplicationRecord
  include Concerns::Common
  belongs_to :contact
end
