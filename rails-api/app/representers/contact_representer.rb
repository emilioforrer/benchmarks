require 'representable/json'

class ContactRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :uuid
  property :name
  property :created_at
  property :updated_at
  collection :emails do
    property :id
    property :uuid
    property :contact_id
    property :account
    property :created_at
    property :updated_at
  end
  collection :phones do
    property :id
    property :uuid
    property :contact_id
    property :number
    property :created_at
    property :updated_at
  end
  collection :addresses do
    property :id
    property :uuid
    property :contact_id
    property :description
    property :created_at
    property :updated_at
  end
end