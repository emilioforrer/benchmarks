defmodule PhoenixApiWeb.ContactView do
  use PhoenixApiWeb, :view
  alias PhoenixApiWeb.ContactView

  def render("index.json", %{contacts: contacts}) do
    %{data: render_many(contacts, ContactView, "contact.json")}
  end

  def render("show.json", %{contact: contact}) do
    %{data: render_one(contact, ContactView, "contact.json")}
  end

  def render("email.json", %{contact: email}) do
    %{
      id: email.id,
      uuid: email.uuid,
      contact_id: email.contact_id,
      account: email.account,
      created_at: email.created_at,
      updated_at: email.updated_at,
    }
  end

  def render("phone.json", %{contact: phone}) do
    %{
      id: phone.id,
      uuid: phone.uuid,
      contact_id: phone.contact_id,
      number: phone.number,
      created_at: phone.created_at,
      updated_at: phone.updated_at,
    }
  end

  def render("address.json", %{contact: address}) do
    %{
      id: address.id,
      uuid: address.uuid,
      contact_id: address.contact_id,
      description: address.description,
      created_at: address.created_at,
      updated_at: address.updated_at,
    }
  end

  def render("contact.json", %{contact: contact}) do
    %{
      id: contact.id,
      uuid: contact.uuid,
      name: contact.name,
      emails: render_many(contact.emails, ContactView, "email.json"),
      phones: render_many(contact.phones, ContactView, "phone.json"),
      addresses: render_many(contact.addresses, ContactView, "address.json"),
      created_at: contact.created_at,
      updated_at: contact.updated_at,
    }
  end
end
