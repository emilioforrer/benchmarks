defmodule PhoenixApi.Accounts.Email do
  use Ecto.Schema
  import Ecto.Changeset
  
  @timestamps_opts inserted_at: :created_at
  
  schema "emails" do
    field :uuid,        :string
    field :account,     :string
    belongs_to :contact, PhoenixApi.Accounts.Contact
    timestamps()
  end

  @doc false
  def changeset(email, attrs) do
    email
    |> cast(attrs, [])
    |> validate_required([])
  end
end
