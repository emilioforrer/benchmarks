defmodule PhoenixApi.Accounts.Contact do
  use Ecto.Schema
  import Ecto.Changeset
  
  @timestamps_opts inserted_at: :created_at

  schema "contacts" do
    field :uuid, :string
    field :name, :string
    has_many :emails, PhoenixApi.Accounts.Email
    has_many :phones, PhoenixApi.Accounts.Phone
    has_many :addresses, PhoenixApi.Accounts.Address
    timestamps()
  end

  @doc false
  def changeset(contact, attrs) do
    contact
    |> cast(attrs, [])
    |> validate_required([])
  end
end
