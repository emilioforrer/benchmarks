defmodule PhoenixApi.Accounts.Phone do
  use Ecto.Schema
  import Ecto.Changeset
  
  @timestamps_opts inserted_at: :created_at
  
  schema "phones" do
    field :uuid,        :string
    field :number,      :string
    belongs_to :contact, PhoenixApi.Accounts.Contact
    timestamps()
  end

  @doc false
  def changeset(phone, attrs) do
    phone
    |> cast(attrs, [])
    |> validate_required([])
  end
end
