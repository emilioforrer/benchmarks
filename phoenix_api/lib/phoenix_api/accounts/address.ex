defmodule PhoenixApi.Accounts.Address do
  use Ecto.Schema
  import Ecto.Changeset
  
  @timestamps_opts inserted_at: :created_at

  schema "addresses" do
    field :uuid,        :string
    field :description,     :string
    belongs_to :contact, PhoenixApi.Accounts.Contact
    timestamps()
  end

  @doc false
  def changeset(address, attrs) do
    address
    |> cast(attrs, [])
    |> validate_required([])
  end
end
