#!/bin/bash

# Wait until Postgres is ready
while ! pg_isready -q -h $RAILS_DATABASE_HOST -p 5432 -U $RAILS_DATABASE_USERNAME
do
  echo "$(date) - waiting for database to start"
  sleep 2
done

# Create, migrate, and seed database if it doesn't exist.
if [[ -z `psql -Atqc "\\list $RAILS_DATABASE_USERNAME"` ]]; then
  echo "Database $RAILS_DATABASE_USERNAME does not exist. Creating..."
  createdb -E UTF8 $RAILS_DATABASE_USERNAME -l en_US.UTF-8 -T template0
  mix ecto.migrate
  mix run priv/repo/seeds.exs
  echo "Database $RAILS_DATABASE_USERNAME created."
fi


if [[ $MIX_ENV == "prod" ]]; then
  cd assets && npm install && ./node_modules/webpack/bin/webpack.js  --mode production  &&  cd ../ && mix phx.digest
  elixir --detached -S mix do compile, phx.server
  nginx -t
  echo "Starting nginx..."
  nginx -g 'daemon off;'
else
  exec mix phx.server
fi



exec "$@"
